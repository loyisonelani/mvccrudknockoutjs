﻿using System.Web.Http;
using AutoMapper;
using EmployeeManager.Data;
using EmployeeManager.Data.Repositories;
using EmployeeManager.Model;

namespace EmployeeManager.Controllers
{
    public class EmployeeApiController : ApiController
    {
        public EmployeeApiController(IEmployeeRepository employeeRepository)
        {
            this.employeeRepository = employeeRepository;
        }

        private readonly IEmployeeRepository employeeRepository;

        [HttpGet]
        [Route("api/employees")]
        public IHttpActionResult Get()
        {
            var employees = employeeRepository.GetAll();
            return Ok(employees);
        }

        [HttpGet]
        [Route("api/employees/{id}")]
        public IHttpActionResult Get(int id)
        {
            if (id == 0)
            {
                return BadRequest();
            }

            var employee = employeeRepository.Get(id);
            return Ok(employee);
        }

        [HttpPost]
        [Route("api/employees")]
        public IHttpActionResult Post(CreateEmployeeFormModel model)
        {
            var employee = Mapper.Map<CreateEmployeeFormModel, Employee>(model);
            if (employee == null)
            {
                return NotFound();
            }

            employeeRepository.Add(employee);
             
            return Ok();
        }
          
        [HttpDelete]
        [Route("api/Employees/Delete/{id}")]
        public IHttpActionResult Delete(int id)
        {
            employeeRepository.Delete(id);

            return Ok();
        }
    }
}