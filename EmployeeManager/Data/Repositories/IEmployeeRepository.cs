﻿using EmployeeManager.Data.Infrastructure;

namespace EmployeeManager.Data.Repositories
{
    public interface IEmployeeRepository : IRepository<Employee>
    {
    }
}
