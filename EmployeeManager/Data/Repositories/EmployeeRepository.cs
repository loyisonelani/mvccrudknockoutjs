﻿using EmployeeManager.Data;
using EmployeeManager.Data.Infrastructure;
using EmployeeManager.Data.Repositories;

namespace VehicleData.Repositories
{
    public class EmployeeRepository : RepositoryBase<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    } 
}