﻿using System.Data.Entity.ModelConfiguration;
using EmployeeManager.Data;

namespace Data.Configurations
{
    public class EmployeeConfiguration : EntityTypeConfiguration<Employee>
    {
        public EmployeeConfiguration()
        {
            ToTable("Employees");
            Property(c => c.EmployeeCode).HasMaxLength(50).IsRequired();
            Property(c => c.FirstName).HasMaxLength(250).IsRequired();
            Property(c => c.LastName).HasMaxLength(250).IsOptional();             
        }
    }
}