﻿using System.Configuration;
using System.Data.Entity;
using Data.Configurations;
  
namespace EmployeeManager.Data
{
    public class DataContex : DbContext
    {  
        public DataContex()
                : base(ConfigurationManager.AppSettings["DBConnConnection"])
        {
        }
 
        public virtual void Commit()
        {
            base.SaveChanges();
        }

        public DbSet<Employee> Employees { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        { 
            modelBuilder.Configurations.Add(new EmployeeConfiguration()); 
        }
    }
}