﻿using System; 
  
namespace EmployeeManager.Data.Infrastructure
{
    public interface IDatabaseFactory : IDisposable
    {
        DataContex Get();
    }
}
