﻿using System.Collections.Generic; 
using System.Data.Entity;
using System.Linq; 

namespace EmployeeManager.Data.Infrastructure
{
    public abstract class RepositoryBase<T> where T : class
    {
        private DataContex dataContext;
        private readonly IDbSet<T> dbset;

        protected RepositoryBase(IDatabaseFactory databaseFactory)
        {
            DatabaseFactory = databaseFactory;
            dbset = DataContext.Set<T>();
        }

        protected IDatabaseFactory DatabaseFactory
        {
            get; private set;
        }

        protected DataContex DataContext
        {
            get { return dataContext ?? (dataContext = DatabaseFactory.Get()); }
        }

        public virtual void Add(T entity)
        {
            dbset.Add(entity);
            dataContext.SaveChanges();
        }

        public virtual void Update(T entity)
        {
            dbset.Attach(entity);
            dataContext.Entry(entity).State = EntityState.Modified;
            dataContext.SaveChanges();
        }

        public virtual void Delete(T entity)
        {
            dbset.Remove(entity);
            dataContext.SaveChanges();
        }

        public virtual void Delete(int id)
        {
            dbset.Remove(Get(id));
            dataContext.SaveChanges();
        }

        public virtual T Get(int id)
        {
            return dbset.Find(id);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return dbset.ToList();
        } 
    }
}