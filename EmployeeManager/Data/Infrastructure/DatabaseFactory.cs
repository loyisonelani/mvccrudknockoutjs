﻿namespace EmployeeManager.Data.Infrastructure
{
    public class DatabaseFactory : Disposable, IDatabaseFactory
    {
        private DataContex dataContext;

        public DataContex Get()
        {
            return dataContext ?? (dataContext = new DataContex());
        }

        protected override void DisposeCore()
        {
            if (dataContext != null)
                dataContext.Dispose();
        } 
    }
}