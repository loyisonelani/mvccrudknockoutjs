﻿using System.Collections.Generic;
  
namespace EmployeeManager.Data.Infrastructure
{
    public interface IRepository<T> where T : class
    {
        void Add(T entity);

        void Update(T entity);

        void Delete(T entity);

        void Delete(int id);

        T Get(int Id);
          
        IEnumerable<T> GetAll();         
    }
}