namespace EmployeeManager.Data
{ 
    public class Employee
    { 
        public int Id { get; set; }
        public string EmployeeCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }         
    }
}
