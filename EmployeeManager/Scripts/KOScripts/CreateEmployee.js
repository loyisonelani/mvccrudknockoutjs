﻿$(function () {
    ko.applyBindings(modelView);
});

var modelView = {
    Id: ko.observable(),
    FirstName: ko.observable(),
    LastName: ko.observable(),
    createEmployee: function () {
        try {
            $.ajax({
                url: '/api/Employees',
                type: 'POST',
                dataType: 'json',
                data: ko.toJSON(this),
                contentType: 'application/json',
                success: successCallback,
                error: errorCallback
            });
        } catch (e) {
            window.location.href = '/employee';
        }
    }
}

function successCallback(data) {
    window.location.href = '/employee';
}
function errorCallback(err) {
    window.location.href = '/employee';
}