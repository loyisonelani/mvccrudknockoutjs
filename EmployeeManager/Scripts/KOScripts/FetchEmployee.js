﻿$(function () {
    ko.applyBindings(modelView);
    modelView.viewEmployees();
});

var modelView = {
    Employees: ko.observableArray([]),
    viewEmployees: function () {
        var thisObj = this;
        try {
            $.ajax({
                url: '/api/employees',
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                success: function (data) {
                    thisObj.Employees(data);
                },
                error: function (err) {
                    alert(err.status + " : " + err.statusText);
                }
            });
        } catch (e) {
            window.location.href = '/employee';
        }
    }
}

function successCallback(data) {
    window.location.href = '/employee';
}
function errorCallback(err) {
    window.location.href = '/employee';
}