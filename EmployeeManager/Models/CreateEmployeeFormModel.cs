namespace EmployeeManager.Model
{
    public class CreateEmployeeFormModel
    {
        public int Id { get; set; }
        public string EmployeeCode => string.Concat(FirstName, "-", LastName);
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
