﻿using AutoMapper;
using EmployeeManager.Data;
using EmployeeManager.Model;

namespace EmployeeManager.Mappers
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

        protected override void Configure()
        { 
            CreateMap<Employee, CreateEmployeeFormModel>();
            CreateMap<Employee, UpdateEmployeeFormModel>(); 
        }
    }
}