﻿using AutoMapper;
using EmployeeManager.Data;
using EmployeeManager.Model;

namespace EmployeeManager.Mappers
{
    class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }

        protected override void Configure()
        { 
            CreateMap<CreateEmployeeFormModel, Employee>();
            CreateMap<UpdateEmployeeFormModel, Employee>(); 
        }
    }
}